﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AuthenticationApp.Models;

namespace AuthenticationApp.Controllers
{
    
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Dashboard");
            }
            return View();
        }
        [HttpPost]
        public ActionResult Login(User model)
        {
            if (model.Username == "admin" && model.Password == "123456")
            {
                FormsAuthentication.SetAuthCookie(model.Username,false);
                return RedirectToAction("Dashboard");
            }
            return View();
        }
        [Authorize]
        public ActionResult Dashboard()
        {
            return View();
        }

        
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Account");
        }
    }
}